import css from "./Divider.scss"

const Divider = () => (
  <div className={css.container}>
    <div className={css.line}></div>
    <div className={css.icon}>
      <img src="static/feather.svg" alt="Feather" />
    </div>
    <div className={css.line}></div>
  </div>
)

export default Divider;