import Link from "next/link";
import css from "./Footer.scss";
import Divider from "./Divider";

const Footer = () => (
  <div className={css.container}>
    <Divider />
    <div className={css.footer}>
      <div className={css.footerLeft}>
        <div className={css.copyright}>© 2018 1von4</div>
        <div className={css.tagline}>Initiative 1von4kindern.at</div>
      </div>
      <div className={css.footerRight}>
        <Link href="/static/impressum.pdf"><a>Impressum</a></Link>
      </div>
    </div>
  </div>
);

export default Footer;
