import {Component} from "react";
import css from "./Hero.scss";
import ScrollIndicator from "./ScrollIndicator";
// import Sprite from "./Sprite";
import dynamic from 'next/dynamic';
import { TweenMax } from "gsap";
const Sprite = dynamic(() => import('./Sprite'), { ssr: false });

class Hero extends Component {

  loadingFinished() {
    TweenMax.to(`.${css.scrollWrapper}`, 0.5, {
      opacity: 1
    });
  }

  render() {
    const progress = this.props.progress;
    let spriteJsonPath = "/static/sprite/sprite.json";
    if (typeof window !== "undefined" && window.innerWidth < 970) {
      spriteJsonPath = "/static/sprite_mobile/sprite.json";
    }
    // let spriteJsonPath = "/static/sprite_high/sprite.json";
    // if (typeof window !== "undefined" && window.location.hash === "#hd") {
    //   spriteJsonPath = ["/static/sprite_multi/sprite-0.json","/static/sprite_multi/sprite-1.json","/static/sprite_multi/sprite-2.json","/static/sprite_multi/sprite-3.json"];
    // } else if (typeof window !== "undefined" && window.location.hash === "#huge") {
    //   spriteJsonPath = "/static/sprite_high/sprite.json";
    // }
    let y = 0;
    if (progress > 1) {
      y = ((progress * 3000) - 3000) * -1;
    }
    return (
      <div className={css.container}>
        <div className={css.sprite} style={{transform: `translateY(${y}px)`}}>
          <Sprite loadingFinishedFn={this.loadingFinished} progress={progress} spriteJsonPath={spriteJsonPath} />
          <div className={css.correction}></div>
          <div className={css.feather}></div>
        </div>
        <div className={css.scrollWrapper}>
          <ScrollIndicator progress={progress} />
        </div>
      </div>
    );
  }

}

export default Hero;
