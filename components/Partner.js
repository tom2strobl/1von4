import css from "./Partner.scss";

const Partner = props => {
  return (
    <div className={css.container}>
      <div className={css.solidimage} style={{backgroundImage: `url(${props.solidimage}`}}></div>
      <div className={css.greyimage} style={{backgroundImage: `url(${props.greyimage}`}}></div>
      <div className={css.copy}>
        <img src={props.logo} alt={props.name}/>
        <p>{props.description}</p>
        <a href={props.link}>Unterstützen</a>
      </div>
    </div>
  );
};

export default Partner;
