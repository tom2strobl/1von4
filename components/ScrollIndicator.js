import css from "./ScrollIndicator.scss"

const ScrollIndicator = ({progress}) => (
  <div className={css.container} style={{opacity: 1-(progress*10)}}>
    <div className={css.copy}>Scroll</div>
    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="63" viewBox="0 0 27 63">
      <g fill="none" fillRule="evenodd" transform="rotate(90 13 12.5)">
        <path stroke="#848D9D" strokeWidth="2" d="M0.62539203,12 C0.62539203,12 1.0676373,12 1.62278586,12 L48.3786103,12"/>
        <polygon fill="#848D9D" points="48 17 48 7 53 12"/>
        <circle cx="50" cy="12" r="12" stroke="#848D9D" strokeWidth="2"/>
      </g>
    </svg>
  </div>
);

export default ScrollIndicator;
