import css from "./Sprite.scss";
import React, {Component} from "react";
import times from "lodash/times";
import isArray from "lodash/isArray";
import TweenMax from "gsap";

class Sprite extends Component {

  shouldComponentUpdate(nextProps) {
    if (this.props.progress !== nextProps.progress) {
      this.setToProgress(nextProps.progress);
    }
    return false;
  }

  componentDidMount() {
    import("pixi.js").then(this.initPixi.bind(this));
  }

  initPixi(PIXI) {
    this.PIXI = PIXI;
    // we don't like all that noise in the logs
    PIXI.utils.skipHello();
    // init pixi app
    this.pixi = new PIXI.Application({
      width: window.innerWidth,
      height: window.innerHeight,
      resolution: window.devicePixelRatio,
      autoResize: true,
      transparent: true,
      // backgroundColor: this.props.backgroundColor,
      antialias: true
    });
    // add resize binding
    window.addEventListener("resize", this.onResize.bind(this));
    // add our stage to the dom
    this.container.appendChild(this.pixi.view);
    // load sprite
    if (isArray(this.props.spriteJsonPath)) {
      this.loadMultiSprite();
    } else if (typeof this.props.spriteJsonPath === "string") {
      this.loadSingleSprite();
    }
  }

  loadSingleSprite() {
    this.pixi.loader.add("sprite", this.props.spriteJsonPath);
    this.pixi.loader.load(this.loadingFinished.bind(this));
  }

  loadMultiSprite() {
    this.props.spriteJsonPath.forEach((spriteSheetPath, i) => {
      this.pixi.loader.add(`sprite${i}`, spriteSheetPath);
    });
    this.pixi.loader.load(this.loadingFinished.bind(this));
  }

  loadingFinished() {
    this.props.loadingFinishedFn();
    this.hideLoading();
    this.loaded = true;
    // if we have more than two resources loaded, we were running multi
    if (Object.keys(this.pixi.loader.resources).length > 2) {
      // multi sprite is split up into multiple parts, so we need to merge them into one sprite to play
      const frames = times(this.props.frameCount + 1, i => this.PIXI.Texture.fromFrame(`1von4_${i}.jpg`));
      this.sprite = new this.PIXI.extras.AnimatedSprite(frames);
    } else {
      // single sprite is easy
      this.sprite = new this.PIXI.extras.AnimatedSprite(this.pixi.loader.resources["sprite"].spritesheet.animations["1von4"]);
    }
    // create the sprite and add it to the stage
    this.sprite.anchor.set(0.5);
    this.sprite.interactive = false;
    this.pixi.stage.addChild(this.sprite);
    this.resizeSprite();
    // set to current progress
    this.setToProgress(this.props.progress);
  }

  resizeSprite() {
    this.sprite.position = new this.PIXI.Point(window.innerWidth / 2, window.innerHeight / 2);
    let scale = 0.5;
    // if (this.sprite._texture.orig.width > window.innerWidth) {
    if (window.innerWidth < 815) {
      scale = window.innerWidth / this.sprite._texture.orig.width;
    }
    this.sprite.scale = new this.PIXI.Point(scale, scale);
    this.container.style.height = `${window.innerHeight}px`;
  }

  setToProgress(progress) {
    // console.log(progress);
    if (!this.sprite) {
      return false;
    }
    // cap at 1
    if (progress > 1) progress = 1;
    const frameIndex = Math.round(this.props.frameCount * progress);
    this.sprite.gotoAndStop(frameIndex);
  }

  onResize() {
    this.pixi.renderer.resize(window.innerWidth, window.innerHeight);
    this.resizeSprite();
  }

  hideLoading() {
    TweenMax.to("#heroloading", 0.5, {
      opacity: 0
    });
  }

  render() {
    return (
      <div className={css.container} ref={r => this.container = r} style={{height: window.innerHeight}}>
        <div className={css.loading} id="heroloading">
          <div className={css.spinner}>
            <div className={css.dot1}></div>
            <div className={css.dot2}></div>
          </div>
        </div>
      </div>
    );
  }

}

Sprite.defaultProps = {
  progress: 0,
  frameCount: 117,
  spriteJsonPath: "",
  backgroundColor: 0xE5EBF6
};

export default Sprite;
