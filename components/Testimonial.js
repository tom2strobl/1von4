import {Component} from "react";
import PropTypes from "prop-types";
import css from "./Testimonial.scss";
import Observer from '@researchgate/react-intersection-observer';
import TweenMax, {Expo, Sine} from "gsap";

class Testimonial extends Component {
  render() {
    const {anchor, mood, image, bio, title, description} = this.props;
    const options = {
      onChange: e => {
        if (e.isIntersecting && this.introed !== true) {
          TweenMax.from(`.${css.mood}`, 1, {
            ease: Sine.easeInOut,
            x: this.props.anchor === "left" ? -100 : 100,
          });
          TweenMax.from(`.${css.image}`, 1, {
            ease: Expo.easeInOut,
            y: -50,
          });
          TweenMax.from(`.${css.info}`, 1.5, {
            ease: Expo.easeInOut,
            y: -100,
          });
          this.introed = true;
        }
      }
    };
    return (
      <Observer {...options}>
        <div className={`${css.container} ${css[anchor]}`}>
          <div className={css.mood} style={{backgroundImage: `url(${mood})`}}></div>
          <div className={css.entity}>
            {/* <div className={css.image} style={{backgroundImage: `url(${image})`}}></div> */}
            <div className={css.image}></div>
            <div className={css.info}>
              <p className={css.bio}>{bio}</p>
              <div className={css.boxBottom}>
                <h3 className={css.title}>{title}</h3>
                <span className={css.description}>{description}</span>
              </div>
            </div>
          </div>
        </div>
      </Observer>
    );
  }
}

Testimonial.propTypes = {
  anchor: PropTypes.string,
  mood: PropTypes.string,
  image: PropTypes.string,
  bio: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string
};

Testimonial.defaultProps = {
  anchor: "left"
};

export default Testimonial;
