import React from 'react'
import Head from "next/head";
import App, { Container } from 'next/app'

export default class MyApp extends App {
  static async getInitialProps ({ Component, ctx }) {
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    return { pageProps }
  }

  render () {
    const { Component, pageProps } = this.props;
    const title = `1von4`;
    const description = `Jedes vierte Kind in Österreich hat psychische Probleme. Nur wenige werden entsprechend behandelt. Das liegt vor allem am fehlenden Wissen über das Beratungsangebot, wie eine aktuelle Studie der Initiative #1von4Kindern und marketagent.com zeigt. Mit Hilfe einer Bewusstseinskampagne sollen Menschen im Umfeld der betroffenen Kinder und Jugendlichen dazu bewegt werden, Hilfe zu suchen.`;
    const url = `http://1von4kinder.at`;
    const image = `${url}/static/shareimage.jpg`;
    return (
      <Container>
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <meta name="robots" content="index,follow" />
          <meta name="googlebot" content="index,follow" />
          <meta name="geo.placename" content="Vienna, Austria" />
          <meta property="og:locale" content="en_US" />
          <meta name="rating" content="General" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <link rel="stylesheet" href="https://use.typekit.net/tji4zup.css" />
          <script src="/static/scripts/modernizr.js" />
          <title>Initiative 1 von 4 Kindern</title>

          <meta property="description" content={description} />
          <meta property="og:type" content="website" />
          <meta property="og:site_name" content={title} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="og:url" content={url} />
          <meta property="og:image" content={image} />
          <meta property="og:image:width" content="1300" />
          <meta property="og:image:height" content="732" />
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:title" content={title} />
          <meta name="twitter:description" content={description} />
          <meta name="twitter:image" content={image} />

          <link rel="apple-touch-icon" sizes="180x180" href="/static/favicons/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/static/favicons/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/static/favicons/favicon-16x16.png" />
          <link rel="manifest" href="/static/favicons/site.webmanifest" />
          <link rel="mask-icon" href="/static/favicons/safari-pinned-tab.svg" color="#f04323" />
          <link rel="shortcut icon" href="/static/favicons/favicon.ico" />
          <meta name="msapplication-TileColor" content="#f04323" />
          <meta name="msapplication-config" content="/static/favicons/browserconfig.xml" />
          <meta name="theme-color" content="#f04323" />
        </Head>
        <Component {...pageProps} />
        <style jsx global>{`
          /* minireset.css v0.0.3 | MIT License | github.com/jgthms/minireset.css */
          html,body,p,ol,ul,li,dl,dt,dd,blockquote,figure,fieldset,legend,textarea,pre,iframe,hr,h1,h2,h3,h4,h5,h6{margin:0;padding:0}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal}ul{list-style:none}button,input,select,textarea{margin:0}html{box-sizing:border-box}*,*:before,*:after{box-sizing:inherit}img,embed,iframe,object,audio,video{height:auto;max-width:100%}iframe{border:0}table{border-collapse:collapse;border-spacing:0}td,th{padding:0;text-align:left}
        `}</style>
      </Container>
    )
  }
}