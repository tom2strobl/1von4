import React, {Component} from "react";
import css from "./Index.scss";
import Divider from "../components/Divider";
import Testimonial from "../components/Testimonial";
import Footer from "../components/Footer";
import Partner from "../components/Partner";
import Hero from "../components/Hero";
import Observer from '@researchgate/react-intersection-observer';
import TweenMax, {Expo, Sine} from "gsap";

if(typeof window !== "undefined") {
  require('intersection-observer');
}

const PartnerList = [{
  name: "die möwe",
  logo: "static/diemoewe.png",
  solidimage: "static/diemoewe_1.jpg",
  greyimage: "static/diemoewe_2.jpg",
  description: "Das zentrale Anliegen der möwe ist der Schutz von Kindern vor Gewalt und ihren Folgen Seit 30 Jahren bieten wir an 6 Standorten in Wien und Niederösterreich Kindern, Jugendlichen und ihren Familien professionelle Hilfe bei körperlichen, seelischen und sexuellen Gewalterfahrungen. Mit Information und Aufklärung schaffen wir Bewusstsein und ermutigen hinzuschauen um Gewalt von vornherein zu verhindern.",
  link: "https://www.die-moewe.at/"
}, {
  name: "Caritas Familienzentrum Wien",
  logo: "static/caritas.png",
  solidimage: "static/caritas_1.jpg",
  greyimage: "static/caritas_2.jpg",
  description: "Ein multiprofessionelles Team, bestehend aus SozialarbeiterInnen, PsychologInnen, PädagogInnen, JuristInnen und PsychotherapeutInnen, unterstützt mit Beratung und Psychotherapie, Kinder, Jugendliche und Erwachsene bei der Bewältigung von seelischen Belastungen und Krisen, stärkt Ressourcen und hilft neue Lösungen zu finden.",
  link: "https://www.caritas-wien.at/hilfe-angebote/kinder-familie/familienzentren-beratung-und-psychotherapie/familienzentrum-wien/"
}, {
  name: "Neunerhaus",
  logo: "static/neunerhaus.svg",
  solidimage: "static/neunerhaus_1.jpg",
  greyimage: "static/neunerhaus_2.jpg",
  description: "Im neunerhaus Café kannst du nicht nur gemütlich Café trinken, zu Mittag essen oder dich ohne Druck, etwas zu bestellen, aufhalten. Hier gibt es auf Wunsch auch Beratung – kostenlos und anonym. Wir nehmen uns Zeit für dich und deine Fragen. Egal, wie alt du bist. Wir sind für dich da.",
  link: "https://www.neunerhaus.at/hilfe/beratung/"
}];

class Index extends Component {

  state = {
    heroProgress: 0
  }

  constructor(props) {
    super(props);
    this.onScroll = this.onScroll.bind(this);
    this.introObserver = this.introObserver.bind(this);
    this.mainObserver = this.mainObserver.bind(this);
    this.moreObserver = this.moreObserver.bind(this);
  }

  onScroll() {
    const progress = window.pageYOffset / 3000;
    this.setState({heroProgress: progress });
  }

  componentDidMount() {
    document.addEventListener("scroll", this.onScroll);
  }

  introObserver(e) {
    if (e.isIntersecting && this.introIntroed !== true) {
      TweenMax.from(`.${css.intro}`, 1.5, {
        ease: Expo.easeOut,
        x: -50,
      });
      TweenMax.from(`.${css.headline}`, 2, {
        ease: Expo.easeOut,
        x: -50,
      });
      this.introIntroed = true;
    }
  }

  mainObserver(e) {
    if (e.isIntersecting && this.mainIntroed !== true) {
      TweenMax.from(`.${css.mainIntro}`, 1.5, {
        ease: Expo.easeOut,
        x: -50,
      });
      TweenMax.from(`.${css.mainCopy}`, 2, {
        ease: Expo.easeOut,
        x: -50,
      });
      TweenMax.from(`.${css.sidebar}`, 1.25, {
        ease: Expo.easeOut,
        x: -50,
      });
      this.mainIntroed = true;
    }
  }

  moreObserver(e) {
    if (e.isIntersecting && this.moreIntroed !== true) {
      TweenMax.from(`.${css.moreIntro}`, 2, {
        ease: Expo.easeOut,
        x: -50,
      });
      TweenMax.from(`.${css.moreCopy}`, 1.5, {
        ease: Expo.easeOut,
        x: -50,
      });
      this.moreIntroed = true;
    }
  }

  render() {
    return (
      <div className={css.container}>
        <Hero progress={this.state.heroProgress} />
        <Observer onChange={this.introObserver}>
          <div className={css.wrapper}>
            <div className={css.intro}>
              Mama, ich hab Angst.<br />
              Papa, ich kann nicht schlafen.<br />
              … mach, dass das Kopfweh weggeht.<br />
              Bitte lass mich nicht alleine …
            </div>
            <Divider />
            <h1 className={css.headline}>1 von 4 Kindern in Österreich trägt unsichtbare Wunden in sich.</h1>
          </div>
        </Observer>
        <Testimonial
          anchor="left"
          mood="/static/mood_1.jpg"
          image=""
          bio="Psychische Probleme sind für 23% der Kinder und Jugendlichen in Österreich Alltag. Also für fast 1 von 4 Kindern. "
          title="Ergebnisse der Marketagent-Studie"
          description="2018"
        />
        <div className={css.wrapper}>
          <Observer onChange={this.mainObserver}>
            <div className={css.main}>
            {/* <div className={css.sidebar}>Haben Sie das übersehen? Genauso verhält es sich mit der Wahrnehmung von psychischen Erkrankungen bei Kindern und Jugendlichen.</div> */}
              <div className={css.mainContent}>
                <img src="static/feather.svg" alt="Feather" />
                <div className={css.mainIntro}>Denn 1 von 4 Kindern ist psychisch krank. Die Spätfolgen von Stress, Angst, Panik und Sucht schneiden sich tief in die Kinderseele. Gemeinsam können wir Ihrem Kind helfen.</div>
                <div className={css.mainCopy}>

                  <div className={css.paragraph}>
                    <div className={css.side}>Erkennen</div>Probleme wirken sich auf die Seele aus und verändern so das Denken, Fühlen und Handeln. Auch von Kindern. In Österreich ist schon jedes vierte Kind von psychischen Problemen betroffen, die oftmals unentdeckt bleiben. Zum einen, weil sie tabuisiert werden und zum anderen, weil sie oft als nichtig abgetan werden.
                  </div>
                  <div className={css.paragraph}>Umso wichtiger ist es aufzuklären, dass das Kind eben nicht gerade eine schwierige Phase hat, einfach trotzig ist oder sich andauernde Kopfschmerzen nur einbildet.</div>
                  <div className={css.paragraph}>Unruhe, Konzentrationsschwierigkeiten, Traurigkeit, Leere, Angst, aber auch psychosomatische Symptome, wie Kopfschmerzen oder Magen-Darm-Beschwerden können ernsthafte Anzeichen eines Leidensdrucks sein.</div>
                  <div className={css.paragraph}>
                    <div className={css.side}>Behandeln</div>In Österreich haben sich viele Organisationen in den Dienst der Kinder gestellt und helfen Betroffenen, einen Ausweg aus der Krankheit zu finden.
                  </div>
                  <div className={css.paragraph}>
                    <div className={css.side}>Helfen</div>Machen Sie den ersten Schritt. Suchen Sie Rat. Reden Sie darüber. Mit geschulten, erfahrenen ExpertInnen, die Ihnen Sicherheit bei der Diagnose geben können.
                  </div>

                  <p>Die Ihnen helfen können, die Wunden an der Seele Ihres Kindes zu heilen.</p>

                  <p>
                    Denn kein Kind soll seiner Angst alleine sein.<br />
                    Kein Kind soll Panik und Stress ertragen müssen.<br />
                    Kein Kind soll mit Sucht kämpfen.
                  </p>
                  <p>Kämpfen wir gemeinsam für eine gesunde Kinderseele.</p>
                </div>
              </div>
            </div>
          </Observer>
        </div>
        {/* <Testimonial
          anchor="right"
          mood="/static/mood_2.jpg"
          image="/static/martinaebm.png"
          bio="Mauris non tempor quam, et lacinia sapien. Mauris accumsan eros eget libero posuere vulputate."
          title="Martina Ebm"
          description="Schauspielerin"
        />
        <div className={css.wrapper}>
          <Observer onChange={this.moreObserver}>
            <div className={css.moreInfo}>
              <div className={css.moreIntro}>1 von 4 Kindern leidet entweder an Angststörungen, an Panik, an Stress oder an Suchtverhalten.</div>
              <div className={css.moreCopy}>
                <p>Deswegen wurde diese Initiative gestartet, die es sich zum Ziel setzt, Bewusstsein für die seelische Qual von Kindern zu schaffen. Und Betroffenen die Möglichkeit zu geben, einfach und rasch Hilfe zu bekommen.</p>
                <p>Denn Kinder sollten in eine unbeschwerte Zukunft gehen können und ihre Seelen ohne Risse bleiben. Und wenn ihre Seelen Risse bekommen, diese ganz schnell wieder verheilen.</p>
              </div>
            </div>
          </Observer>
        </div> */}

        <div className={css.partner}>
          {PartnerList.map(partner => <Partner key={partner.name} {...partner} />)}
        </div>

        <div className={css.wrapper}>
          <Footer />
        </div>
      </div>
    );
  }

}

export default Index;
